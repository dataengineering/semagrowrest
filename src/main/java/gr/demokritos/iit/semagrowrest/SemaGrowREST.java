package gr.demokritos.iit.semagrowrest;

import java.io.IOException;
import java.io.PrintWriter;

import javax.json.Json;
import javax.json.JsonObject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openrdf.query.BindingSet;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.QueryLanguage;
import org.openrdf.query.TupleQuery;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.http.HTTPRepository;

/**
 * Servlet implementation class SemaGrowREST
 */
public class SemaGrowREST extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SemaGrowREST() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// Set the response message's MIME type.
		response.setContentType("text/html;charset=UTF-8");
		// Allocate a output writer to write the response message into the network socket.
		PrintWriter out = response.getWriter();

		String q = request.getParameter("q");
		if (q==null) {
			out.println("you must specify query using the parameter \"q\"");
			return;
		}
		
		String limit = request.getParameter("page_size");
		if (limit == null) {
			limit = "100";//default limit is 100
		}
		
		String query_str = " PREFIX xsd: <http://www.w3.org/2001/XMLSchema> "
				+ "SELECT ?s ?p ?o "
				+ "{ "
				+ " ?s ?p ?o  ";
		
		if ( ! q.equals("*") ) {
			query_str += " FILTER ( (regex(str(?s), \"" + q
					+ "\", \"i\")) || (regex(str(?p), \"" + q
					+ "\", \"i\")) || (regex(str(?o), \"" + q + "\", \"i\")) ) ";
		}
		query_str += "} "
				+ " LIMIT " + limit;
		
		Repository repository = new HTTPRepository("http://143.233.226.42:8080/SemaGrow/sparql");

		try {
			RepositoryConnection connection = repository.getConnection();
			TupleQuery query = connection.prepareTupleQuery(QueryLanguage.SPARQL, query_str);

	        TupleQueryResult results =  query.evaluate();
	        while (results.hasNext()) {
	        	
	        	BindingSet bindingSet = results.next();
	        	
	        	JsonObject jo = Json.createObjectBuilder()
	        			.add("s", bindingSet.getValue("s").stringValue())
	        			.add("p", bindingSet.getValue("p").stringValue())
	        			.add("o", bindingSet.getValue("o").stringValue())
	        			.build();
	        	
	        	out.println(jo.toString());
	        	out.println();

	        }
	        results.close();
		} catch (RepositoryException e) {
			e.printStackTrace();
		} catch (MalformedQueryException e) {
			e.printStackTrace();
		} catch (QueryEvaluationException e) {
			e.printStackTrace();
		}
		
	}


}
